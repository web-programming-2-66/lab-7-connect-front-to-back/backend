import { IsEmail, IsNotEmpty, Length } from 'class-validator';
export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Length(4, 32)
  fullName: string;

  @IsNotEmpty()
  @Length(4, 16)
  password: string;

  // @IsArray()
  // @ArrayNotEmpty()
  // roles: ('admin' | 'user')[];

  @IsNotEmpty()
  gender: 'male' | 'female' | 'others';
}
